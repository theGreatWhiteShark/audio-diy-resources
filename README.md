This repo will contain a number of helpful links and resource
dedicated to audio DIY hardware and electronic projects

## Resources 

### Filter
- https://www.allaboutcircuits.com/technical-articles/low-pass-filter-tutorial-basics-passive-RC-filter/ 
  (nice introduction into the fundamental concepts of filtering and
  how to construct a low pass from resistors and capacitors)

### Operational amplifier

#### entry points for beginners
- http://education.lenardaudio.com/en/12_amps_2.html (accessible but
  in depth explanation how both op-amps and audio amplifiers in
  general do work. Highly recommended)
- https://www.youtube.com/watch?v=7FYHt5XviKc Nice introduction by EEVblog
- [blog post: what is an op amp](https://e2e.ti.com/blogs_/b/analogwire/archive/2020/01/21/what-is-an-op-amp)

### more advanced but still accessible resources (provided by the manufacturer)
- [The Signal - a compendium of blog posts on op amp design topics by Bruce Trump](https://www.ti.com/lit/ml/slyt701/slyt701.pdf)
- [TL072 data sheet](https://www.ti.com/lit/ds/symlink/tl072.pdf)
- [Understanding op amp specifications and data sheets](https://www.ti.com/lit/an/sloa011a/sloa011a.pdf)
- [Handbook of op amp applications](https://www.ti.com/lit/an/sboa092b/sboa092b.pdf)


### further resources
- https://en.wikipedia.org/wiki/Operational_amplifier
- https://www.elprocus.com/types-of-amplifiers-with-workings/
- https://ask.audio/articles/how-to-build-your-first-amplifier
- https://www.ece.ucsb.edu/Faculty/rodwell/Classes/ece2c/labs/Lab%201a%20-%202C%202007.pdf
- https://circuitdigest.com/electronic-circuits/small-loudspeaker-circuit-diagram
  (minimal example with circuit diagram and audio-specific)



## Devices available at Dezentrale
### op-apm
- [TL072](https://www.ti.com/lit/ds/symlink/tl072.pdf)

### multimeter
- [Voltcraft VC 220](http://www.produktinfo.conrad.com/datenblaetter/100000-124999/121414-an-01-de-Digital_Multimeter_VC_220.pdf)

### power supply
- [Voltcraft PS 2403 pro](https://asset.conrad.com/media10/add/160267/c1/-/gl/000511816ML03/manual-511816-voltcraft-vlp-2403pro-bench-psu-adjustable-voltage-0-40-v-dc-0-3-a-252-w-no-of-outputs-3-x.pdf)
- [Owon DP3032](https://owontechnology.eu/odp3032.html)

### oscilloscope
- [Voltcraft VC 630-2](https://www.conrad.com/p/voltcraft-vc-630-2-analog-30-mhz-2-channel-122421)
- [Gw Instek GDS-1072A-U](https://www.gwinstek.ca/products/1897/70mhz-2-channel-digital-storage-oscilloscope)
- [Metex MXG-9802A](https://www.tequipment.net/MetexMXG-9802A.html)

## Former sessions

### 12.02.2020

We made a big practical session and tried to get out op-amp to work
on a breadboard.

### 04.02.2020

We repeated the basics of the op-amp and explored the fundamental
concepts of [low pass filters](https://www.allaboutcircuits.com/technical-articles/low-pass-filter-tutorial-basics-passive-RC-filter/).

### 28.01.2020
We explored the [basic concepts of an
op-amp](http://education.lenardaudio.com/en/12_amps_2.html) including
the [voltage divider](https://en.wikipedia.org/wiki/Voltage_divider)
and the basic usage of an oscilloscope. Afterwards we put everything
together and added the op-amp into the circuit we constructed last
time.

### 21.01.2020
We talked about how to calculate voltages and currents in circuits
using [Kirchhoffs' circuit
law](https://en.wikipedia.org/wiki/Kirchhoff%27s_circuit_laws). Started
with some fundamental concepts of operational amplifiers and build our
first circuits by connecting a cinch input to a loudspeaker.

### 14.01.2020
We talked about the basics of current, voltage, and resistors
(wikipedia stuff only).

We decided that a basic reverb will be the first audio-unit we will
try to
understand. https://www.tubesandmore.com/sites/all/modules/custom/tech_corner/img/spring_reverbs_marshall_avt50x.svg
will be used as references and we will approach this rather complex
thingy by breaking it apart and understanding the individual parts
first.
